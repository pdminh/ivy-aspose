[Ivy]
1692E88699920888 3.20 #module
>Proto >Proto Collection #zClass
Ct0 CreateAndViewDocument Big #zClass
Ct0 B #cInfo
Ct0 #process
Ct0 @TextInP .resExport .resExport #zField
Ct0 @TextInP .type .type #zField
Ct0 @TextInP .processKind .processKind #zField
Ct0 @AnnotationInP-0n ai ai #zField
Ct0 @MessageFlowInP-0n messageIn messageIn #zField
Ct0 @MessageFlowOutP-0n messageOut messageOut #zField
Ct0 @TextInP .xml .xml #zField
Ct0 @TextInP .responsibility .responsibility #zField
Ct0 @StartRequest f0 '' #zField
Ct0 @EndTask f1 '' #zField
Ct0 @RichDialog f3 '' #zField
Ct0 @RichDialog f5 '' #zField
Ct0 @PushWFArc f2 '' #zField
Ct0 @TaskSwitchSimple f7 '' #zField
Ct0 @TkArc f8 '' #zField
Ct0 @PushWFArc f4 '' #zField
Ct0 @TaskSwitchSimple f9 '' #zField
Ct0 @TkArc f10 '' #zField
Ct0 @PushWFArc f6 '' #zField
>Proto Ct0 Ct0 CreateAndViewDocument #zField
Ct0 f0 outLink start.ivp #txt
Ct0 f0 type ivy.aspose.Data #txt
Ct0 f0 inParamDecl '<> param;' #txt
Ct0 f0 actionDecl 'ivy.aspose.Data out;
' #txt
Ct0 f0 guid 1692E8869D98886C #txt
Ct0 f0 requestEnabled true #txt
Ct0 f0 triggerEnabled false #txt
Ct0 f0 callSignature start() #txt
Ct0 f0 caseData businessCase.attach=true #txt
Ct0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ct0 f0 @C|.responsibility Everybody #txt
Ct0 f0 81 49 30 30 -21 17 #rect
Ct0 f0 @|StartRequestIcon #fIcon
Ct0 f1 type ivy.aspose.Data #txt
Ct0 f1 689 49 30 30 0 15 #rect
Ct0 f1 @|EndIcon #fIcon
Ct0 f3 targetWindow NEW #txt
Ct0 f3 targetDisplay TOP #txt
Ct0 f3 richDialogId ivy.aspose.CreateDocument #txt
Ct0 f3 startMethod start() #txt
Ct0 f3 type ivy.aspose.Data #txt
Ct0 f3 requestActionDecl '<> param;' #txt
Ct0 f3 responseActionDecl 'ivy.aspose.Data out;
' #txt
Ct0 f3 responseMappingAction 'out=in;
out.documentId=result.documentId;
' #txt
Ct0 f3 isAsynch false #txt
Ct0 f3 isInnerRd false #txt
Ct0 f3 userContext '* ' #txt
Ct0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create Document</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f3 264 42 112 44 -48 -8 #rect
Ct0 f3 @|RichDialogIcon #fIcon
Ct0 f5 targetWindow NEW #txt
Ct0 f5 targetDisplay TOP #txt
Ct0 f5 richDialogId ivy.aspose.ViewDocument #txt
Ct0 f5 startMethod start(java.lang.Long) #txt
Ct0 f5 type ivy.aspose.Data #txt
Ct0 f5 requestActionDecl '<java.lang.Long documentId> param;' #txt
Ct0 f5 requestMappingAction 'param.documentId=in.documentId;
' #txt
Ct0 f5 responseActionDecl 'ivy.aspose.Data out;
' #txt
Ct0 f5 responseMappingAction 'out=in;
' #txt
Ct0 f5 isAsynch false #txt
Ct0 f5 isInnerRd false #txt
Ct0 f5 userContext '* ' #txt
Ct0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>View Document</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ct0 f5 520 42 112 44 -43 -8 #rect
Ct0 f5 @|RichDialogIcon #fIcon
Ct0 f2 expr out #txt
Ct0 f2 632 64 689 64 #arcP
Ct0 f7 actionDecl 'ivy.aspose.Data out;
' #txt
Ct0 f7 actionTable 'out=in1;
' #txt
Ct0 f7 outTypes "ivy.aspose.Data" #txt
Ct0 f7 outLinks "TaskA.ivp" #txt
Ct0 f7 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=Create Document
TaskA.PRI=2
TaskA.ROL=Everybody
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0' #txt
Ct0 f7 type ivy.aspose.Data #txt
Ct0 f7 template "" #txt
Ct0 f7 177 49 30 30 0 16 #rect
Ct0 f7 @|TaskSwitchSimpleIcon #fIcon
Ct0 f8 expr out #txt
Ct0 f8 type ivy.aspose.Data #txt
Ct0 f8 var in1 #txt
Ct0 f8 111 64 177 64 #arcP
Ct0 f4 expr data #txt
Ct0 f4 outCond ivp=="TaskA.ivp" #txt
Ct0 f4 207 64 264 64 #arcP
Ct0 f9 actionDecl 'ivy.aspose.Data out;
' #txt
Ct0 f9 actionTable 'out=in1;
' #txt
Ct0 f9 outTypes "ivy.aspose.Data" #txt
Ct0 f9 outLinks "TaskA.ivp" #txt
Ct0 f9 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=View Document
TaskA.PRI=2
TaskA.ROL=Everybody
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0' #txt
Ct0 f9 type ivy.aspose.Data #txt
Ct0 f9 template "" #txt
Ct0 f9 433 49 30 30 0 16 #rect
Ct0 f9 @|TaskSwitchSimpleIcon #fIcon
Ct0 f10 expr out #txt
Ct0 f10 type ivy.aspose.Data #txt
Ct0 f10 var in1 #txt
Ct0 f10 376 64 433 64 #arcP
Ct0 f6 expr data #txt
Ct0 f6 outCond ivp=="TaskA.ivp" #txt
Ct0 f6 463 64 520 64 #arcP
>Proto Ct0 .type ivy.aspose.Data #txt
>Proto Ct0 .processKind NORMAL #txt
>Proto Ct0 0 0 32 24 18 0 #rect
>Proto Ct0 @|BIcon #fIcon
Ct0 f5 mainOut f2 tail #connect
Ct0 f2 head f1 mainIn #connect
Ct0 f0 mainOut f8 tail #connect
Ct0 f8 head f7 in #connect
Ct0 f7 out f4 tail #connect
Ct0 f4 head f3 mainIn #connect
Ct0 f3 mainOut f10 tail #connect
Ct0 f10 head f9 in #connect
Ct0 f9 out f6 tail #connect
Ct0 f6 head f5 mainIn #connect
