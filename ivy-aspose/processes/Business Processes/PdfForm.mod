[Ivy]
1730D8FB1AE9F913 3.20 #module
>Proto >Proto Collection #zClass
Pm0 PdfForm Big #zClass
Pm0 B #cInfo
Pm0 #process
Pm0 @TextInP .resExport .resExport #zField
Pm0 @TextInP .type .type #zField
Pm0 @TextInP .processKind .processKind #zField
Pm0 @AnnotationInP-0n ai ai #zField
Pm0 @MessageFlowInP-0n messageIn messageIn #zField
Pm0 @MessageFlowOutP-0n messageOut messageOut #zField
Pm0 @TextInP .xml .xml #zField
Pm0 @TextInP .responsibility .responsibility #zField
Pm0 @StartRequest f0 '' #zField
Pm0 @TaskSwitchSimple f9 '' #zField
Pm0 @EndTask f3 '' #zField
Pm0 @RichDialog f4 '' #zField
Pm0 @TaskSwitchSimple f7 '' #zField
Pm0 @RichDialog f6 '' #zField
Pm0 @PushWFArc f8 '' #zField
Pm0 @TkArc f10 '' #zField
Pm0 @PushWFArc f11 '' #zField
Pm0 @PushWFArc f13 '' #zField
Pm0 @TkArc f1 '' #zField
>Proto Pm0 Pm0 PdfForm #zField
Pm0 f0 outLink start.ivp #txt
Pm0 f0 type ivy.aspose.PdfFormData #txt
Pm0 f0 inParamDecl '<> param;' #txt
Pm0 f0 actionDecl 'ivy.aspose.PdfFormData out;
' #txt
Pm0 f0 guid 1730D8FB1C275076 #txt
Pm0 f0 requestEnabled true #txt
Pm0 f0 triggerEnabled false #txt
Pm0 f0 callSignature start() #txt
Pm0 f0 caseData businessCase.attach=true #txt
Pm0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Pm0 f0 @C|.responsibility Everybody #txt
Pm0 f0 81 49 30 30 -21 17 #rect
Pm0 f0 @|StartRequestIcon #fIcon
Pm0 f9 actionDecl 'ivy.aspose.PdfFormData out;
' #txt
Pm0 f9 actionTable 'out=in1;
' #txt
Pm0 f9 outTypes "ivy.aspose.PdfFormData" #txt
Pm0 f9 outLinks "TaskA.ivp" #txt
Pm0 f9 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=View Document
TaskA.PRI=2
TaskA.ROL=Everybody
TaskA.SKIP_TASK_LIST=true
TaskA.TYPE=0' #txt
Pm0 f9 type ivy.aspose.PdfFormData #txt
Pm0 f9 template "" #txt
Pm0 f9 465 49 30 30 0 16 #rect
Pm0 f9 @|TaskSwitchSimpleIcon #fIcon
Pm0 f3 type ivy.aspose.PdfFormData #txt
Pm0 f3 721 49 30 30 0 15 #rect
Pm0 f3 @|EndIcon #fIcon
Pm0 f4 targetWindow NEW #txt
Pm0 f4 targetDisplay TOP #txt
Pm0 f4 richDialogId ivy.aspose.CreatePdfForm #txt
Pm0 f4 startMethod start() #txt
Pm0 f4 type ivy.aspose.PdfFormData #txt
Pm0 f4 requestActionDecl '<> param;' #txt
Pm0 f4 responseActionDecl 'ivy.aspose.PdfFormData out;
' #txt
Pm0 f4 responseMappingAction 'out=in;
out.documentId=result.documentId;
' #txt
Pm0 f4 isAsynch false #txt
Pm0 f4 isInnerRd false #txt
Pm0 f4 userContext '* ' #txt
Pm0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create PDF form</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pm0 f4 296 42 112 44 -46 -8 #rect
Pm0 f4 @|RichDialogIcon #fIcon
Pm0 f7 actionDecl 'ivy.aspose.PdfFormData out;
' #txt
Pm0 f7 actionTable 'out=in1;
' #txt
Pm0 f7 outTypes "ivy.aspose.PdfFormData" #txt
Pm0 f7 outLinks "TaskA.ivp" #txt
Pm0 f7 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=Create Document
TaskA.PRI=2
TaskA.ROL=Everybody
TaskA.SKIP_TASK_LIST=true
TaskA.TYPE=0' #txt
Pm0 f7 type ivy.aspose.PdfFormData #txt
Pm0 f7 template "" #txt
Pm0 f7 209 49 30 30 0 16 #rect
Pm0 f7 @|TaskSwitchSimpleIcon #fIcon
Pm0 f6 targetWindow NEW #txt
Pm0 f6 targetDisplay TOP #txt
Pm0 f6 richDialogId ivy.aspose.ViewDocument #txt
Pm0 f6 startMethod start(java.lang.Long) #txt
Pm0 f6 type ivy.aspose.PdfFormData #txt
Pm0 f6 requestActionDecl '<java.lang.Long documentId> param;' #txt
Pm0 f6 requestMappingAction 'param.documentId=in.documentId;
' #txt
Pm0 f6 responseActionDecl 'ivy.aspose.PdfFormData out;
' #txt
Pm0 f6 responseMappingAction 'out=in;
out.documentId=in.documentId;
' #txt
Pm0 f6 isAsynch false #txt
Pm0 f6 isInnerRd false #txt
Pm0 f6 userContext '* ' #txt
Pm0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>View Document</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Pm0 f6 552 42 112 44 -43 -8 #rect
Pm0 f6 @|RichDialogIcon #fIcon
Pm0 f8 expr data #txt
Pm0 f8 outCond ivp=="TaskA.ivp" #txt
Pm0 f8 239 64 296 64 #arcP
Pm0 f10 expr out #txt
Pm0 f10 type ivy.aspose.PdfFormData #txt
Pm0 f10 var in1 #txt
Pm0 f10 408 64 465 64 #arcP
Pm0 f11 expr data #txt
Pm0 f11 outCond ivp=="TaskA.ivp" #txt
Pm0 f11 495 64 552 64 #arcP
Pm0 f13 expr out #txt
Pm0 f13 664 64 721 64 #arcP
Pm0 f1 expr out #txt
Pm0 f1 type ivy.aspose.PdfFormData #txt
Pm0 f1 var in1 #txt
Pm0 f1 111 64 209 64 #arcP
>Proto Pm0 .type ivy.aspose.PdfFormData #txt
>Proto Pm0 .processKind NORMAL #txt
>Proto Pm0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Pm0 0 0 32 24 18 0 #rect
>Proto Pm0 @|BIcon #fIcon
Pm0 f6 mainOut f13 tail #connect
Pm0 f13 head f3 mainIn #connect
Pm0 f7 out f8 tail #connect
Pm0 f8 head f4 mainIn #connect
Pm0 f4 mainOut f10 tail #connect
Pm0 f10 head f9 in #connect
Pm0 f9 out f11 tail #connect
Pm0 f11 head f6 mainIn #connect
Pm0 f0 mainOut f1 tail #connect
Pm0 f1 head f7 in #connect
