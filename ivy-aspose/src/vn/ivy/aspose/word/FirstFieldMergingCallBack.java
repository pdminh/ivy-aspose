package vn.ivy.aspose.word;

import java.io.ByteArrayInputStream;

import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.addons.docfactory.aspose.AsposeFieldMergingCallback;

import com.aspose.words.DocumentBuilder;
import com.aspose.words.FieldMergingArgs;
import com.aspose.words.ImageFieldMergingArgs;
import com.aspose.words.MergeFieldImageDimension;
import com.aspose.words.Paragraph;

public class FirstFieldMergingCallBack extends AsposeFieldMergingCallback {
	
	private int width;
	private int height;
	

	public FirstFieldMergingCallBack(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}

	@Override
	public void fieldMerging(FieldMergingArgs fieldMergingArgs) throws Exception {
		if (fieldMergingArgs.getFieldValue() == null) {
			removeBlankLine(fieldMergingArgs);
			return;
		}
	}

	@Override
	public void imageFieldMerging(ImageFieldMergingArgs imageFieldMergingArgs) throws Exception {
		// The field value is a byte array, just cast it and create a stream on it.
		ByteArrayInputStream imageStream = new ByteArrayInputStream((byte[]) imageFieldMergingArgs.getFieldValue());
		// Now the mail merge engine will retrieve the image from the stream.
		imageFieldMergingArgs.setImageStream(imageStream);
		imageFieldMergingArgs.setImageWidth(new MergeFieldImageDimension(this.width));
		imageFieldMergingArgs.setImageHeight(new MergeFieldImageDimension(this.height));
	}

	private void removeBlankLine(FieldMergingArgs fieldMergingArgs) throws Exception {
		DocumentBuilder builder = new DocumentBuilder(fieldMergingArgs.getDocument());
		builder.moveToMergeField(fieldMergingArgs.getFieldName());
		Paragraph paragraph = builder.getCurrentParagraph();
		if (StringUtils.isBlank(paragraph.getText())) {
			paragraph.remove();
		}
	}

}
