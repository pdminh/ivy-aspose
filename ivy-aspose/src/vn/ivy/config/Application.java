package vn.ivy.config;

import java.io.InputStream;

import ch.ivyteam.ivy.ThirdPartyLicenses;
import ch.ivyteam.ivy.addons.docfactory.aspose.AsposeProduct;
import ch.ivyteam.ivy.environment.Ivy;

import com.aspose.words.License;

public class Application {
	{
		License license = new License();
		try {
			InputStream in = ThirdPartyLicenses.getDocumentFactoryLicense();
			ch.ivyteam.ivy.addons.docfactory.aspose.LicenseLoader.loadLicenseforProduct(AsposeProduct.WORDS);
			license.setLicense(in);
			in.close();
		} catch (Exception e) {
			Ivy.log().error("Aspose Word Licence error", e);
		}
		
		Ivy.log().debug("Aspose licence set up successful");
	}
}
