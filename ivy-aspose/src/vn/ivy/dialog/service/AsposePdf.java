package vn.ivy.dialog.service;

import java.io.IOException;

import com.aspose.pdf.Border;
import com.aspose.pdf.CheckboxField;
import com.aspose.pdf.ComboBoxField;
import com.aspose.pdf.Dash;
import com.aspose.pdf.Document;
import com.aspose.pdf.Rectangle;
import com.aspose.pdf.TextBoxField;

import aspose.pdf.FormField;
import aspose.pdf.FormFieldType;
import aspose.pdf.Pdf;
import aspose.pdf.Section;
import ch.ivyteam.ivy.addons.docfactory.aspose.AsposeProduct;
import ch.ivyteam.ivy.addons.docfactory.aspose.LicenseLoader;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.workflow.document.IDocument;
import ch.ivyteam.ivy.workflow.document.Path;

public class AsposePdf {

	private String name;
	private Long documentId;

	public void init() {
		// Load license for aspose slides & cell
		try {
			LicenseLoader.loadLicenseforProduct(AsposeProduct.PDF);
		} catch (Exception e) {
			Ivy.log().error(e);
		}
	}
	
	public void createPdfForm() throws Exception {
		String bindingName = name.trim().replace(" ", "_");
		ch.ivyteam.ivy.scripting.objects.File tempFileIvy = null;
		try {
			// Create temporary file
			tempFileIvy = new ch.ivyteam.ivy.scripting.objects.File("Hello_" + bindingName + ".pdf", true);
		} catch (IOException e) {
			Ivy.log().error(e);
		}
		
		Document pdfDocument = new Document();
		pdfDocument.getPages().add();
		
		// Create a field
		TextBoxField textBoxField1 = new TextBoxField(pdfDocument.getPages().get_Item(1), new Rectangle(100, 200, 300, 300));
		// Set the field name
		textBoxField1.setPartialName("textbox1");
		// Set the field value
		textBoxField1.setValue("Text Box");
		// Create a border object
		Border border = new Border(textBoxField1);
		// Set the border width
		border.setWidth(5);
		// Set the border dash style
		border.setDash(new Dash(1, 1));
		// Set the field border
		textBoxField1.setBorder(border);
		// Add the field to the document
		pdfDocument.getForm().add(textBoxField1, 1);
		
		
		
		
		// instantiate ComboBox Field object
		ComboBoxField combo = new ComboBoxField(pdfDocument.getPages().get_Item(1), new Rectangle(100, 600, 150, 616));
		// add option to ComboBox
		combo.addOption("Red");
		combo.addOption("Yellow");
		combo.addOption("Green");
		combo.addOption("Blue");
		// add combo box object to form fields collection of document object
		pdfDocument.getForm().add(combo,1);
		
		CheckboxField checkbox = new CheckboxField(pdfDocument.getPages().get_Item(1), new Rectangle(50, 100, 150, 150));
		checkbox.setPartialName("abc");
		checkbox.setChecked(true);
		checkbox.setWidth(50);
		checkbox.setHeight(50);
		
		Border checkBoxBorder = new Border(checkbox);
		// Set the border width
		border.setWidth(5);
		// Set the border dash style
		border.setDash(new Dash(1, 1));
		
		checkbox.setBorder(checkBoxBorder);
		
		pdfDocument.getForm().add(checkbox, 1);
		
		// Save the modified PDF
		pdfDocument.save(tempFileIvy.getAbsolutePath());


		// Save to ivy database
		IDocument document = Ivy.wfCase().documents()
										.add(new Path("KindOfDocument/" + tempFileIvy.getName()))
										.write()
										.withContentFrom(tempFileIvy.getJavaFile());
		
		// Get document id for the next dialog
		documentId = document.getId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

}
