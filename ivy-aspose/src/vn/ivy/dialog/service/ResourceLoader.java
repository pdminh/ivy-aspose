package vn.ivy.dialog.service;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ResourceLoader {
	/**
	 * Get resource from a folder.
	 * 
	 * @param relativeToClassesFolder
	 * @return path to folder
	 */
	public Path getResource(String relativeToClassesFolder) {
		try {
			return Paths.get(getClass().getClassLoader().getResource(relativeToClassesFolder).toURI());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
