[Ivy]
1692E8DD8BB2567E 3.20 #module
>Proto >Proto Collection #zClass
Cs0 CreateDocumentProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Cs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @MessageFlowInP-0n messageIn messageIn #zField
Cs0 @MessageFlowOutP-0n messageOut messageOut #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @RichDialogInitStart f0 '' #zField
Cs0 @RichDialogProcessEnd f1 '' #zField
Cs0 @RichDialogMethodStart f3 '' #zField
Cs0 @GridStep f6 '' #zField
Cs0 @PushWFArc f7 '' #zField
Cs0 @GridStep f8 '' #zField
Cs0 @PushWFArc f9 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @RichDialogEnd f10 '' #zField
Cs0 @RichDialogProcessEnd f4 '' #zField
Cs0 @PushWFArc f5 '' #zField
Cs0 @RichDialogMethodStart f11 '' #zField
Cs0 @PushWFArc f12 '' #zField
>Proto Cs0 Cs0 CreateDocumentProcess #zField
Cs0 f0 guid 1692E8DD9FCF9CE4 #txt
Cs0 f0 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f0 method start() #txt
Cs0 f0 disableUIEvents true #txt
Cs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f0 outParameterDecl '<java.lang.Long documentId> result;
' #txt
Cs0 f0 outParameterMapAction 'result.documentId=in.service.documentId;
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f0 83 51 26 26 -16 15 #rect
Cs0 f0 @|RichDialogInitStartIcon #fIcon
Cs0 f1 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f1 339 51 26 26 0 12 #rect
Cs0 f1 @|RichDialogProcessEndIcon #fIcon
Cs0 f3 guid 1692EB4370C3736E #txt
Cs0 f3 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f3 method createDocument() #txt
Cs0 f3 disableUIEvents false #txt
Cs0 f3 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f3 outParameterDecl '<> result;
' #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>createDocument()</name>
    </language>
</elementInfo>
' #txt
Cs0 f3 83 147 26 26 -49 15 #rect
Cs0 f3 @|RichDialogMethodStartIcon #fIcon
Cs0 f6 actionDecl 'ivy.aspose.CreateDocument.CreateDocumentData out;
' #txt
Cs0 f6 actionTable 'out=in;
' #txt
Cs0 f6 actionCode in.service.createHelloWord(); #txt
Cs0 f6 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create document</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f6 168 138 112 44 -47 -8 #rect
Cs0 f6 @|StepIcon #fIcon
Cs0 f7 expr out #txt
Cs0 f7 109 160 168 160 #arcP
Cs0 f8 actionDecl 'ivy.aspose.CreateDocument.CreateDocumentData out;
' #txt
Cs0 f8 actionTable 'out=in;
' #txt
Cs0 f8 actionCode in.service.init(); #txt
Cs0 f8 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f8 168 42 112 44 -8 -8 #rect
Cs0 f8 @|StepIcon #fIcon
Cs0 f9 expr out #txt
Cs0 f9 109 64 168 64 #arcP
Cs0 f2 expr out #txt
Cs0 f2 280 64 339 64 #arcP
Cs0 f10 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f10 guid 1692EBC7D34FEB93 #txt
Cs0 f10 331 243 26 26 0 12 #rect
Cs0 f10 @|RichDialogEndIcon #fIcon
Cs0 f4 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f4 339 147 26 26 0 12 #rect
Cs0 f4 @|RichDialogProcessEndIcon #fIcon
Cs0 f5 expr out #txt
Cs0 f5 280 160 339 160 #arcP
Cs0 f11 guid 1692FDDF1E9EF795 #txt
Cs0 f11 type ivy.aspose.CreateDocument.CreateDocumentData #txt
Cs0 f11 method close() #txt
Cs0 f11 disableUIEvents false #txt
Cs0 f11 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f11 outParameterDecl '<> result;
' #txt
Cs0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close()</name>
    </language>
</elementInfo>
' #txt
Cs0 f11 83 243 26 26 -19 15 #rect
Cs0 f11 @|RichDialogMethodStartIcon #fIcon
Cs0 f12 expr out #txt
Cs0 f12 109 256 331 256 #arcP
>Proto Cs0 .type ivy.aspose.CreateDocument.CreateDocumentData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f3 mainOut f7 tail #connect
Cs0 f7 head f6 mainIn #connect
Cs0 f0 mainOut f9 tail #connect
Cs0 f9 head f8 mainIn #connect
Cs0 f8 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f6 mainOut f5 tail #connect
Cs0 f5 head f4 mainIn #connect
Cs0 f11 mainOut f12 tail #connect
Cs0 f12 head f10 mainIn #connect
