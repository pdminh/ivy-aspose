[Ivy]
1730D9051833D0DD 3.20 #module
>Proto >Proto Collection #zClass
Cs0 CreatePdfFormProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Cs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @MessageFlowInP-0n messageIn messageIn #zField
Cs0 @MessageFlowOutP-0n messageOut messageOut #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @RichDialogInitStart f0 '' #zField
Cs0 @RichDialogProcessEnd f1 '' #zField
Cs0 @RichDialogEnd f4 '' #zField
Cs0 @GridStep f6 '' #zField
Cs0 @PushWFArc f7 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @RichDialogMethodStart f8 '' #zField
Cs0 @GridStep f9 '' #zField
Cs0 @PushWFArc f11 '' #zField
Cs0 @PushWFArc f13 '' #zField
>Proto Cs0 Cs0 CreatePdfFormProcess #zField
Cs0 f0 guid 1730D9051A748A3A #txt
Cs0 f0 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f0 method start() #txt
Cs0 f0 disableUIEvents true #txt
Cs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f0 outParameterDecl '<java.lang.Long documentId> result;
' #txt
Cs0 f0 outParameterMapAction 'result.documentId=in.service.documentId;
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f0 83 51 26 26 -16 15 #rect
Cs0 f0 @|RichDialogInitStartIcon #fIcon
Cs0 f1 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f1 339 51 26 26 0 12 #rect
Cs0 f1 @|RichDialogProcessEndIcon #fIcon
Cs0 f4 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f4 guid 1730D9051BA9A87D #txt
Cs0 f4 339 179 26 26 0 12 #rect
Cs0 f4 @|RichDialogEndIcon #fIcon
Cs0 f6 actionDecl 'ivy.aspose.CreatePdfForm.CreatePdfFormData out;
' #txt
Cs0 f6 actionTable 'out=in;
' #txt
Cs0 f6 actionCode in.service.init(); #txt
Cs0 f6 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f6 168 42 112 44 -8 -8 #rect
Cs0 f6 @|StepIcon #fIcon
Cs0 f7 expr out #txt
Cs0 f7 109 64 168 64 #arcP
Cs0 f2 expr out #txt
Cs0 f2 280 64 339 64 #arcP
Cs0 f8 guid 1730D9253813475F #txt
Cs0 f8 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f8 method createPdfForm() #txt
Cs0 f8 disableUIEvents false #txt
Cs0 f8 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f8 outParameterDecl '<> result;
' #txt
Cs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>createDocument()</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f8 83 179 26 26 -49 15 #rect
Cs0 f8 @|RichDialogMethodStartIcon #fIcon
Cs0 f9 actionDecl 'ivy.aspose.CreatePdfForm.CreatePdfFormData out;
' #txt
Cs0 f9 actionTable 'out=in;
' #txt
Cs0 f9 actionCode in.service.createPdfForm(); #txt
Cs0 f9 type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
Cs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create document</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f9 168 170 112 44 -47 -8 #rect
Cs0 f9 @|StepIcon #fIcon
Cs0 f11 expr out #txt
Cs0 f11 109 192 168 192 #arcP
Cs0 f13 expr out #txt
Cs0 f13 280 192 339 192 #arcP
>Proto Cs0 .type ivy.aspose.CreatePdfForm.CreatePdfFormData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f0 mainOut f7 tail #connect
Cs0 f7 head f6 mainIn #connect
Cs0 f6 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f8 mainOut f11 tail #connect
Cs0 f11 head f9 mainIn #connect
Cs0 f9 mainOut f13 tail #connect
Cs0 f13 head f4 mainIn #connect
