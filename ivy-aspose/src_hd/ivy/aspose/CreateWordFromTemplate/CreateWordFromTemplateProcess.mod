[Ivy]
16923D67D608D61F 3.20 #module
>Proto >Proto Collection #zClass
Cs0 CreateWordFromTemplateProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Cs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @MessageFlowInP-0n messageIn messageIn #zField
Cs0 @MessageFlowOutP-0n messageOut messageOut #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @RichDialogInitStart f0 '' #zField
Cs0 @RichDialogProcessEnd f1 '' #zField
Cs0 @RichDialogMethodStart f8 '' #zField
Cs0 @RichDialogProcessEnd f10 '' #zField
Cs0 @GridStep f9 '' #zField
Cs0 @PushWFArc f12 '' #zField
Cs0 @PushWFArc f11 '' #zField
Cs0 @RichDialogMethodStart f3 '' #zField
Cs0 @RichDialogProcessEnd f4 '' #zField
Cs0 @GridStep f5 '' #zField
Cs0 @PushWFArc f6 '' #zField
Cs0 @PushWFArc f7 '' #zField
Cs0 @GridStep f13 '' #zField
Cs0 @PushWFArc f14 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @RichDialogMethodStart f15 '' #zField
Cs0 @RichDialogProcessEnd f16 '' #zField
Cs0 @GridStep f18 '' #zField
Cs0 @PushWFArc f19 '' #zField
Cs0 @PushWFArc f17 '' #zField
Cs0 @GridStep f20 '' #zField
Cs0 @RichDialogProcessEnd f21 '' #zField
Cs0 @RichDialogProcessStart f22 '' #zField
Cs0 @PushWFArc f23 '' #zField
Cs0 @PushWFArc f24 '' #zField
Cs0 @RichDialogMethodStart f25 '' #zField
Cs0 @GridStep f26 '' #zField
Cs0 @RichDialogProcessEnd f27 '' #zField
Cs0 @PushWFArc f28 '' #zField
Cs0 @PushWFArc f29 '' #zField
Cs0 @RichDialogProcessEnd f30 '' #zField
Cs0 @GridStep f31 '' #zField
Cs0 @RichDialogMethodStart f32 '' #zField
Cs0 @PushWFArc f33 '' #zField
Cs0 @PushWFArc f34 '' #zField
Cs0 @RichDialogMethodStart f35 '' #zField
Cs0 @GridStep f36 '' #zField
Cs0 @RichDialogProcessEnd f37 '' #zField
Cs0 @PushWFArc f38 '' #zField
Cs0 @PushWFArc f39 '' #zField
>Proto Cs0 Cs0 CreateWordFromTemplateProcess #zField
Cs0 f0 guid 16923D67E685C598 #txt
Cs0 f0 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f0 method start() #txt
Cs0 f0 disableUIEvents true #txt
Cs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f0 outParameterDecl '<> result;
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 83 51 26 26 -16 15 #rect
Cs0 f0 @|RichDialogInitStartIcon #fIcon
Cs0 f1 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f1 339 51 26 26 0 12 #rect
Cs0 f1 @|RichDialogProcessEndIcon #fIcon
Cs0 f8 guid 16923DCAC0E6392E #txt
Cs0 f8 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f8 method download() #txt
Cs0 f8 disableUIEvents false #txt
Cs0 f8 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f8 outParameterDecl '<> result;
' #txt
Cs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download()</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f8 83 243 26 26 -31 15 #rect
Cs0 f8 @|RichDialogMethodStartIcon #fIcon
Cs0 f10 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f10 339 243 26 26 0 12 #rect
Cs0 f10 @|RichDialogProcessEndIcon #fIcon
Cs0 f9 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f9 actionTable 'out=in;
' #txt
Cs0 f9 actionCode in.service.download(in.service.createDocument()); #txt
Cs0 f9 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f9 168 234 112 44 -27 -8 #rect
Cs0 f9 @|StepIcon #fIcon
Cs0 f12 expr out #txt
Cs0 f12 280 256 339 256 #arcP
Cs0 f11 expr out #txt
Cs0 f11 109 256 168 256 #arcP
Cs0 f3 guid 1692424EDB01FA1B #txt
Cs0 f3 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f3 method downloadMultiFile() #txt
Cs0 f3 disableUIEvents false #txt
Cs0 f3 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f3 outParameterDecl '<> result;
' #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>downloadMultiFile()</name>
    </language>
</elementInfo>
' #txt
Cs0 f3 83 339 26 26 -53 15 #rect
Cs0 f3 @|RichDialogMethodStartIcon #fIcon
Cs0 f4 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f4 339 339 26 26 0 12 #rect
Cs0 f4 @|RichDialogProcessEndIcon #fIcon
Cs0 f5 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f5 actionTable 'out=in;
' #txt
Cs0 f5 actionCode 'in.service.download( in.service.createMultiDocument());' #txt
Cs0 f5 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f5 168 330 112 44 -27 -8 #rect
Cs0 f5 @|StepIcon #fIcon
Cs0 f6 expr out #txt
Cs0 f6 280 352 339 352 #arcP
Cs0 f7 expr out #txt
Cs0 f7 109 352 168 352 #arcP
Cs0 f13 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f13 actionTable 'out=in;
' #txt
Cs0 f13 actionCode in.service.init(); #txt
Cs0 f13 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f13 168 42 112 44 0 -8 #rect
Cs0 f13 @|StepIcon #fIcon
Cs0 f14 expr out #txt
Cs0 f14 109 64 168 64 #arcP
Cs0 f2 expr out #txt
Cs0 f2 280 64 339 64 #arcP
Cs0 f15 guid 169245E1E62513D5 #txt
Cs0 f15 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f15 method addNewExpectation() #txt
Cs0 f15 disableUIEvents false #txt
Cs0 f15 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f15 outParameterDecl '<> result;
' #txt
Cs0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>addNewExpectation()</name>
    </language>
</elementInfo>
' #txt
Cs0 f15 83 147 26 26 -58 15 #rect
Cs0 f15 @|RichDialogMethodStartIcon #fIcon
Cs0 f16 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f16 339 147 26 26 0 12 #rect
Cs0 f16 @|RichDialogProcessEndIcon #fIcon
Cs0 f18 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f18 actionTable 'out=in;
' #txt
Cs0 f18 actionCode 'in.service.expectations.add(in.service.newExpectation);
in.service.newExpectation = "";' #txt
Cs0 f18 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f18 168 138 112 44 0 -8 #rect
Cs0 f18 @|StepIcon #fIcon
Cs0 f19 expr out #txt
Cs0 f19 109 160 168 160 #arcP
Cs0 f17 expr out #txt
Cs0 f17 280 160 339 160 #arcP
Cs0 f20 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f20 actionTable 'out=in;
' #txt
Cs0 f20 actionCode 'in.service.image = in.uploadEvent.getFile().getContents();
in.service.imageName = in.uploadEvent.getFile().getFileName();
in.service.ivyFile = in.service.getIvyFileFromByteArray(in.uploadEvent.getFile().getFileName(),in.uploadEvent.getFile().getContents());' #txt
Cs0 f20 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Process upload file</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f20 168 426 112 44 -53 -8 #rect
Cs0 f20 @|StepIcon #fIcon
Cs0 f21 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f21 339 435 26 26 0 12 #rect
Cs0 f21 @|RichDialogProcessEndIcon #fIcon
Cs0 f22 guid 169281086A2B905A #txt
Cs0 f22 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f22 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f22 actionTable 'out=in;
out.uploadEvent=event as org.primefaces.event.FileUploadEvent;
' #txt
Cs0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>fileUpload</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f22 83 435 26 26 -28 15 #rect
Cs0 f22 @|RichDialogProcessStartIcon #fIcon
Cs0 f23 expr out #txt
Cs0 f23 109 448 168 448 #arcP
Cs0 f24 expr out #txt
Cs0 f24 280 448 339 448 #arcP
Cs0 f25 guid 16929B86D66AFADD #txt
Cs0 f25 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f25 method downloadPowerPoint() #txt
Cs0 f25 disableUIEvents false #txt
Cs0 f25 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f25 outParameterDecl '<> result;
' #txt
Cs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>downloadPowerPoint()</name>
    </language>
</elementInfo>
' #txt
Cs0 f25 435 243 26 26 -62 15 #rect
Cs0 f25 @|RichDialogMethodStartIcon #fIcon
Cs0 f26 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f26 actionTable 'out=in;
' #txt
Cs0 f26 actionCode in.service.download(in.service.createPowerPoint()); #txt
Cs0 f26 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f26 520 234 112 44 -27 -8 #rect
Cs0 f26 @|StepIcon #fIcon
Cs0 f27 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f27 691 243 26 26 0 12 #rect
Cs0 f27 @|RichDialogProcessEndIcon #fIcon
Cs0 f28 expr out #txt
Cs0 f28 461 256 520 256 #arcP
Cs0 f29 expr out #txt
Cs0 f29 632 256 691 256 #arcP
Cs0 f30 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f30 1043 243 26 26 0 12 #rect
Cs0 f30 @|RichDialogProcessEndIcon #fIcon
Cs0 f31 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f31 actionTable 'out=in;
' #txt
Cs0 f31 actionCode in.service.download(in.service.createExcel()); #txt
Cs0 f31 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f31 872 234 112 44 -27 -8 #rect
Cs0 f31 @|StepIcon #fIcon
Cs0 f32 guid 1692D29B899FCE25 #txt
Cs0 f32 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f32 method downloadExcel() #txt
Cs0 f32 disableUIEvents false #txt
Cs0 f32 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f32 outParameterDecl '<> result;
' #txt
Cs0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>downloadExcel()</name>
    </language>
</elementInfo>
' #txt
Cs0 f32 787 243 26 26 -45 15 #rect
Cs0 f32 @|RichDialogMethodStartIcon #fIcon
Cs0 f33 expr out #txt
Cs0 f33 984 256 1043 256 #arcP
Cs0 f34 expr out #txt
Cs0 f34 813 256 872 256 #arcP
Cs0 f35 guid 16946A81E0CF8858 #txt
Cs0 f35 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f35 method downloadExcelFromStream() #txt
Cs0 f35 disableUIEvents false #txt
Cs0 f35 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f35 outParameterDecl '<> result;
' #txt
Cs0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>downloadExcelFromStream()</name>
    </language>
</elementInfo>
' #txt
Cs0 f35 1139 243 26 26 -80 15 #rect
Cs0 f35 @|RichDialogMethodStartIcon #fIcon
Cs0 f36 actionDecl 'ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData out;
' #txt
Cs0 f36 actionTable 'out=in;
' #txt
Cs0 f36 actionCode in.service.downloadExcelFromStream(); #txt
Cs0 f36 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>download</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f36 1224 234 112 44 -27 -8 #rect
Cs0 f36 @|StepIcon #fIcon
Cs0 f37 type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
Cs0 f37 1395 243 26 26 0 12 #rect
Cs0 f37 @|RichDialogProcessEndIcon #fIcon
Cs0 f38 expr out #txt
Cs0 f38 1336 256 1395 256 #arcP
Cs0 f39 expr out #txt
Cs0 f39 1165 256 1224 256 #arcP
>Proto Cs0 .type ivy.aspose.CreateWordFromTemplate.CreateWordFromTemplateData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f8 mainOut f11 tail #connect
Cs0 f11 head f9 mainIn #connect
Cs0 f9 mainOut f12 tail #connect
Cs0 f12 head f10 mainIn #connect
Cs0 f3 mainOut f7 tail #connect
Cs0 f7 head f5 mainIn #connect
Cs0 f5 mainOut f6 tail #connect
Cs0 f6 head f4 mainIn #connect
Cs0 f0 mainOut f14 tail #connect
Cs0 f14 head f13 mainIn #connect
Cs0 f13 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f15 mainOut f19 tail #connect
Cs0 f19 head f18 mainIn #connect
Cs0 f18 mainOut f17 tail #connect
Cs0 f17 head f16 mainIn #connect
Cs0 f22 mainOut f23 tail #connect
Cs0 f23 head f20 mainIn #connect
Cs0 f20 mainOut f24 tail #connect
Cs0 f24 head f21 mainIn #connect
Cs0 f25 mainOut f28 tail #connect
Cs0 f28 head f26 mainIn #connect
Cs0 f26 mainOut f29 tail #connect
Cs0 f29 head f27 mainIn #connect
Cs0 f32 mainOut f34 tail #connect
Cs0 f34 head f31 mainIn #connect
Cs0 f31 mainOut f33 tail #connect
Cs0 f33 head f30 mainIn #connect
Cs0 f35 mainOut f39 tail #connect
Cs0 f39 head f36 mainIn #connect
Cs0 f36 mainOut f38 tail #connect
Cs0 f38 head f37 mainIn #connect
